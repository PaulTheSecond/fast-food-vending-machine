﻿using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using static DastFoodVendingMachine.Core.Tools.InitialContext;

namespace DastFoodVendingMachine.Core.Common
{
    public class DrinkRepository : IRepository<Drink>
    {
        public Drink ByName(string name)
        {
            return GetDrinks().SingleOrDefault(s=>s.Name == name);
        }

        public void Delete(Drink entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Drink> GetAll()
        {
            return GetDrinks();
        }

        public Drink Save(Drink entity)
        {
            throw new NotImplementedException();
        }

        public Drink Update(Drink entity)
        {
            throw new NotImplementedException();
        }
    }
}
