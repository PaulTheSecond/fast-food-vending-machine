﻿using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using static DastFoodVendingMachine.Core.Tools.InitialContext;

namespace DastFoodVendingMachine.Core.Common
{
    public class FoodRepository : IRepository<Food>
    {
        public Food ByName(string name)
        {
            return GetFood().SingleOrDefault(s=>s.Name == name);
        }

        public void Delete(Food entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Food> GetAll()
        {
            return GetFood();
        }

        public Food Save(Food entity)
        {
            throw new NotImplementedException();
        }

        public Food Update(Food entity)
        {
            throw new NotImplementedException();
        }
    }
}
