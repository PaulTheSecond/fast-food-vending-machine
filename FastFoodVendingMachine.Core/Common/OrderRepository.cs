﻿using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Domain;
using System;
using System.Collections.Generic;
using static DastFoodVendingMachine.Core.Tools.InitialContext;

namespace DastFoodVendingMachine.Core.Common
{
    public class OrderRepository : IRepository<Order>
    {
        public Order ByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Delete(Order entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> GetAll()
        {
            return GetOrders();
        }

        public Order Save(Order entity)
        {
            return SaveOrder(entity);
        }

        public Order Update(Order entity)
        {
            throw new NotImplementedException();
        }
    }
}
