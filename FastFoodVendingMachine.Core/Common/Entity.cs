﻿using DastFoodVendingMachine.Core.Contracts;
using System.Collections.Generic;

namespace DastFoodVendingMachine.Core.Common
{
    public class Entity<TKey> : IEntity<TKey> where TKey : new()
    {
        public virtual TKey Id { get; set; } = new TKey();

        protected bool Equals(Entity<TKey> other)
        {
            return EqualityComparer<TKey>.Default.Equals(Id, other.Id);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<TKey>.Default.GetHashCode(Id);
        }

        public virtual object GetId()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this)) return true;

            var eobj = obj as Entity<TKey>;
            return eobj != null && Equals(eobj);
        }
    }
}
