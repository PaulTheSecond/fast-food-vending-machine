﻿using DastFoodVendingMachine.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace DastFoodVendingMachine.Core.Contracts
{
    public interface IReadonlyRepository<TEntity>
    {
        TEntity ByName(string name);
        IEnumerable<TEntity> GetAll();
    }
}
