﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DastFoodVendingMachine.Core.Contracts
{
    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; set; }
    }

    public interface IEntity
    {
        object GetId();
    }
}
