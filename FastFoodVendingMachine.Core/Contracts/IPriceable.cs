﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DastFoodVendingMachine.Core.Contracts
{
    public interface IPriceable
    {
        decimal GetPrice();
    }
}
