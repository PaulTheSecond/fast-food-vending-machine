﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DastFoodVendingMachine.Core.Contracts
{
    public interface IRepository <TEntity> : IReadonlyRepository<TEntity>
    {
        TEntity Save(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
