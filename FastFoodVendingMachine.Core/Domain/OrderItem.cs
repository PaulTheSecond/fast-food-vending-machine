using DastFoodVendingMachine.Core.Contracts;

namespace DastFoodVendingMachine.Core.Domain
{
    public class OrderItem : IPriceable
    {
        public Food Food { get; set; }
        public Addition FoodAddition { get; set; }
        public double FoodAdditionQuantity { get; set; }

        public Drink Drink { get; set; }
        public Addition DrinkAddition { get; set; }
        public double DrinkAdditionQauntity { get; set; }

        public decimal GetPrice()
        {
            var result = 0m;
            if(Food != null)
            {
                result += Food.GetPrice();
            }
            if(FoodAddition != null)
            {
                result += FoodAddition.GetPrice() * (decimal)FoodAdditionQuantity;
            }
            if(Drink != null)
            {
                result += Drink.GetPrice();
            }
            if(DrinkAddition != null)
            {
                result += DrinkAddition.GetPrice() * (decimal)DrinkAdditionQauntity;
            }

            return result;
        }

        public override string ToString()
        {
            if (Food != null && FoodAddition != null)
            {
                return $"Комплекс: {Drink}, {DrinkAddition}, {Food}, {FoodAddition}";
            }

            return Food?.ToString() ?? FoodAddition?.ToString() ?? Drink?.ToString() ?? DrinkAddition.ToString();
        }
    }
}
