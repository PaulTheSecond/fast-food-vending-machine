﻿using DastFoodVendingMachine.Core.Contracts;

namespace DastFoodVendingMachine.Core.Domain
{
    //public class Product
    //{
    //    public string Name { get; set; }
    //    public decimal Price { get; set; }
    //    public List<Addition> AllowedAdditions { get; set; }
    //}


    public class Addition : Product, IPriceable
    {
        public double MinQuantity { get; set; }
        public double MaxQuantity { get; set; }
    }


    



}
