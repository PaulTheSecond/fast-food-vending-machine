﻿using DastFoodVendingMachine.Core.Common;
using DastFoodVendingMachine.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DastFoodVendingMachine.Core.Domain
{
    public class Order : IPriceable
    {
        public string Number { get; set; }
        public DateTime DateTime { get; set; }
        public List<OrderItem> OrderItems { get; set; }

        public decimal GetPrice()
        {
            return OrderItems.Sum(s => s.GetPrice());
        }
    }


    



}
