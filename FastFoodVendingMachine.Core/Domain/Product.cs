using DastFoodVendingMachine.Core.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace DastFoodVendingMachine.Core.Domain
{
    public abstract class Product : IPriceable
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public ICollection<Addition> Additions { get; private set; }

        public ICollection<Product> Composition { get; private set; }

        public virtual decimal GetPrice()
        {
            if(Price > 0)
                return Price;
            if (Composition != null && Composition.Any())
            {
                return Composition.Sum(s => s.GetPrice());
            }
            return 0;
        }

        public virtual void AddComponent(Product product)
        {
            if(Composition == null)
            {
                Composition = new List<Product>();
            }
            Composition.Add(product);
        }

        public virtual void AddAddition(Addition addition)
        {
            if(Additions == null)
            {
                Additions = new List<Addition>();
            }
            Additions.Add(addition);
        }

        public override string ToString()
        {
            var composition = Composition.Aggregate(string.Empty,
                (res, c) => string.IsNullOrWhiteSpace(res) ? c.ToString() : $"{res}, {c}");
            return $"{Name}({composition})";
        }
    }
}
