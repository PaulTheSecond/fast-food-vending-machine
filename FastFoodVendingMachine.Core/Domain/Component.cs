﻿using DastFoodVendingMachine.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DastFoodVendingMachine.Core.Domain
{
    public class Component : Product, IPriceable
    {
        public double MinQuantity { get; set; }
        public double MaxQuantity { get; set; }
    }
}
