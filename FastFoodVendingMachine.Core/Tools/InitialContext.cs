using DastFoodVendingMachine.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DastFoodVendingMachine.Core.Tools
{
    public static class InitialContext
    {
        private static List<Addition> _components;
        private static List<Drink> _drinks;
        private static List<Food> _food;
        private static List<Order> _orders;

        public static IEnumerable<Addition> GetComponents()
        {
            return _components ?? (_components = new List<Addition>
            {
                new Addition{
                    Name = "Вода",
                    Price = 10,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Кофе",
                    Price = 40,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Молоко",
                    Price = 10,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Молочная пенка",
                    Price = 10,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Сахар",
                    Price = 3,
                    MaxQuantity =5,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Сироп",
                    Price = 3,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Чай черный",
                    Price = 15
                },
                new Addition{
                    Name = "Чай зеленый",
                    Price = 15,
                },
                new Addition{
                    Name = "Ветчина",
                    Price = 15,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Сыр",
                    Price = 10,
                    MaxQuantity =1,
                    MinQuantity =1
                },
                new Addition{
                    Name = "Джем",
                    Price = 5,
                    MaxQuantity =1,
                    MinQuantity =1
                }
            });
        }

        public static IEnumerable<Drink> GetDrinks()
        {
            var components = GetComponents();
            var water = new Drink
            {
                Name = "Вода"
            };
            water.AddComponent(components.SingleOrDefault(s => s.Name == "Вода"));
            water.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            water.Price = water.GetPrice();

            var espresso = new Drink
            {
                Name = "Эспрессо"
            };
            espresso.AddComponent(water);
            espresso.AddComponent(components.SingleOrDefault(s => s.Name == "Кофе"));
            espresso.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            espresso.AddAddition(components.SingleOrDefault(s => s.Name == "Молоко"));
            espresso.AddAddition(components.SingleOrDefault(s => s.Name == "Сироп"));
            espresso.Price = espresso.GetPrice();

            var latte = new Drink
            {
                Name = "Латте"
            };
            latte.AddComponent(espresso);
            latte.AddComponent(components.SingleOrDefault(s => s.Name == "Молоко"));
            latte.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            latte.AddAddition(components.SingleOrDefault(s => s.Name == "Сироп"));
            latte.Price = latte.GetPrice();

            var cappuccino = new Drink
            {
                Name = "Капучино"
            };
            cappuccino.AddComponent(latte);
            cappuccino.AddComponent(components.SingleOrDefault(s => s.Name == "Молочная пенка"));
            cappuccino.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            cappuccino.AddAddition(components.SingleOrDefault(s => s.Name == "Сироп"));
            cappuccino.Price = cappuccino.GetPrice();

            var greenTea = new Drink
            {
                Name = "Зеленый чай"
            };
            greenTea.AddComponent(water);
            greenTea.AddComponent(components.SingleOrDefault(s => s.Name == "Чай зеленый"));
            greenTea.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            greenTea.AddAddition(components.SingleOrDefault(s => s.Name == "Молоко"));
            greenTea.AddAddition(components.SingleOrDefault(s => s.Name == "Сироп"));
            greenTea.Price = greenTea.GetPrice();

            var blackTea = new Drink
            {
                Name = "Черный чай"
            };
            blackTea.AddComponent(water);
            blackTea.AddComponent(components.SingleOrDefault(s => s.Name == "Чай черный"));
            blackTea.AddAddition(components.SingleOrDefault(s => s.Name == "Сахар"));
            blackTea.AddAddition(components.SingleOrDefault(s => s.Name == "Молоко"));
            blackTea.AddAddition(components.SingleOrDefault(s => s.Name == "Сироп"));
            blackTea.Price = blackTea.GetPrice();


            return _drinks ?? (_drinks = new List<Drink> {
                water,
                espresso,
                latte,
                cappuccino,
                blackTea,
                greenTea
             });

        }

        public static IEnumerable<Food> GetFood()
        {
            var components = GetComponents();
            var bread = new Food
            {
                Name = "Хлеб",
                Price = 10
            };
            bread.AddAddition(components.SingleOrDefault(s => s.Name == "Ветчина"));
            bread.AddAddition(components.SingleOrDefault(s => s.Name == "Сыр"));

            var bun = new Food
            {
                Name = "Булочка",
                Price = 15
            };
            bun.AddAddition(components.SingleOrDefault(s => s.Name == "Ветчина"));
            bun.AddAddition(components.SingleOrDefault(s => s.Name == "Сыр"));

            var chips = new Food
            {
                Name = "Чипсы",
                Price = 40
            };

            var cookie = new Food
            {
                Name = "Печенье",
                Price = 33
            };
            cookie.AddAddition(components.SingleOrDefault(s => s.Name == "Джем"));

            return _food ?? (_food = new List<Food> {
                bread,
                bun,
                chips,
                cookie
            });
        }

        public static IEnumerable<Order> GetOrders()
        {
            return _orders ?? (_orders = new List<Order>());
        }

        public static Order SaveOrder(Order order)
        {
            if(_orders == null)
            {
                _orders = new List<Order>();
            }
            _orders.Add(order);
            return order;
        }
    }
}
