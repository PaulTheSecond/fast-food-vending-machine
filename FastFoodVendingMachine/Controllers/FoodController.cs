﻿using Microsoft.AspNetCore.Mvc;
using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Domain;
using DastFoodVendingMachine.Core.Common;

namespace FastFoodVendingMachine.Controllers
{
    [Route("api/[controller]")]
    public class FoodController : Controller
    {
        private IRepository<Food> repo;
        public FoodController()
        {
            repo = new FoodRepository();
        }

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(repo.GetAll());
        }
    }
}
