using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Domain;
using DastFoodVendingMachine.Core.Common;

namespace FastFoodVendingMachine.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private IRepository<Order> repo;
        public OrderController()
        {
            repo = new OrderRepository();
        }

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(repo.GetAll());
        }

        [HttpPost]
        public IActionResult Post([FromBody]Order newItem)
        {
            var res = repo.Save(newItem);
            return Ok(res.ToString());
        }

        [HttpPost]
        [Route("GetTotals")]
        public IActionResult GetTotals([FromBody] Order item)
        {
            var message = item.OrderItems.Aggregate(string.Empty,
                (res, i) => string.IsNullOrWhiteSpace(res) ? i.ToString() : $"{res}, {i}");
            return Ok(new{total = item.GetPrice(), message = message});
        }
    }
}
