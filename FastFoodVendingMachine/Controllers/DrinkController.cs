﻿using Microsoft.AspNetCore.Mvc;
using DastFoodVendingMachine.Core.Contracts;
using DastFoodVendingMachine.Core.Common;
using DastFoodVendingMachine.Core.Domain;

namespace FastFoodVendingMachine.Controllers
{
    [Route("api/[controller]")]
    public class DrinkController : Controller
    {
        private IRepository<Drink> repo;
        public DrinkController()
        {
            repo = new DrinkRepository();
        }

        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(repo.GetAll());
        }
    }
}
