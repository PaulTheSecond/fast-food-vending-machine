﻿/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

"use strict";

const gulp = require('gulp');
const bower = require('gulp-bower');
const series = require('stream-series');
const inject = require('gulp-inject');
const wiredep = require('wiredep').stream;
const del = require('del');
const uglifyjs = require('gulp-uglifyjs');
const concat = require('gulp-concat');
const sequence = require('gulp-sequence');
const imagemin = require('gulp-imagemin');
//const cache = require('gulp-cache');
//const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
//const pngquant = require('imagemin-pngquant');
const foreach = require('gulp-foreach');



const webroot = './wwwroot/',
    frontend = './frontend/';

var paths = {
    appSrc: frontend + "app/app.js",
    ngModule: frontend + "app/modules/**/*.js",
    ngDirective: frontend + "app/directives/**/*.js",
    ngController: frontend + "app/controllers/**/*.js",
    ngFactory: frontend + "app/factories/**/*.js",
    script: frontend + "scripts/**/*.js",
    views: frontend + 'views/**/*.html',
    indexView: frontend + 'views/index.html',
    styles: frontend + "sass/**/*.sass",
    images: frontend + 'img/**/*'
};

gulp.task('bower', function () {
    return bower();   // By default gulp-bower runs install command for Bower
});

gulp.task('styles', function () {
    return gulp.src(paths.styles)
        .pipe(sass({
            includePaths: require('node-bourbon').includePaths
        }).on('error', sass.logError))
        .pipe(rename({ suffix: '.min', prefix: '' }))
        //.pipe(autoprefixer({ browsers: ['last 5 versions'], cascade: false }))
        .pipe(cleanCSS())
        .pipe(gulp.dest(webroot + 'css'))
        .pipe(browserSync.stream());

});

gulp.task('inject:index', function () {
    var commonjsSrc = gulp.src(webroot + 'js/common.min.js', { read: false });
    var applicationSrc = gulp.src(webroot + 'js/application.min.js', { read: false });
    var styleSrc = gulp.src(webroot + 'css/**/*.css');

    gulp.src(paths.indexView)
        .pipe(wiredep({
            optional: 'configuration',
            goes: 'here',
            ignorePath: '../../wwwroot'
        }))
        .pipe(inject(series(commonjsSrc, applicationSrc), { ignorePath: '/wwwroot' }))
        .pipe(inject(series(styleSrc), { ignorePath: '/wwwroot' }))
        .pipe(gulp.dest(webroot));
});

gulp.task('scripts:minapp', function () {
    return gulp.src([paths.ngModule, paths.appSrc, paths.ngDirective, paths.ngController, paths.ngFactory])
        .pipe(uglifyjs('application.min.js'))
        .pipe(gulp.dest(webroot + 'js'));
});

gulp.task('scripts:min', function () {
    return gulp.src(paths.script, { base: frontend })
        .pipe(uglifyjs('common.min.js'))
        .pipe(gulp.dest(webroot + 'js'));
});

gulp.task('clean', function () {
    return del(['wwwroot/**', '!wwwroot', '!wwwroot/libraries/**']);
});

gulp.task('views', ['inject:index'], function () {
    return gulp.src([paths.views, '!' + paths.indexView], { base: frontend })
        .pipe(gulp.dest(webroot));
});

gulp.task('img', function () {
    return gulp.src(paths.images, { base: frontend })
        //.pipe(cache(imagemin({
        //    interlaced: true,
        //    progressive: true,
        //    svgoPlugins: [{ removeViewBox: false }],
        //    use: [pngquant()]
        //})))
        .pipe(gulp.dest(webroot));
});

gulp.task('build', sequence('clean', 'bower', ['scripts:min', 'scripts:minapp', 'styles', 'img'], 'views'));

gulp.task('default', ['watch:sources']);

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: webroot,
            proxy: 'localhost:5000'
        },
        notify: false
    });

    browserSync.watch(webroot + '**/*').on('change', browserSync.reload);
});

//Sources for js develop
//=====================================================================================================================
gulp.task('inject:indexSrc', function () {
    var commonjsSrc = gulp.src(webroot + 'js/scripts/**/*.js', { read: false });
    var applicationSrc = gulp.src(webroot + 'js/app/**/*.js', { read: false });
    var styleSrc = gulp.src(webroot + 'css/**/*.css');

    gulp.src(paths.indexView)
        .pipe(wiredep({
            optional: 'configuration',
            goes: 'here',
            ignorePath: '../../wwwroot'
        }))
        .pipe(inject(series(commonjsSrc, applicationSrc), { ignorePath: '/wwwroot' }))
        .pipe(inject(series(styleSrc), { ignorePath: '/wwwroot' }))
        .pipe(gulp.dest(webroot));
});

gulp.task('scripts:app', function () {
    return gulp.src([paths.ngModule, paths.appSrc, paths.ngController, paths.ngFactory, paths.ngDirective], { base: frontend })
        .pipe(gulp.dest(webroot + 'js'));
});

gulp.task('scripts', function () {
    return gulp.src(paths.script, { base: frontend })
        .pipe(gulp.dest(webroot + 'js'));
});

gulp.task('views:sources', ['inject:indexSrc'], function () {
    return gulp.src([paths.views, '!' + paths.indexView], { base: frontend })
        .pipe(gulp.dest(webroot));
});

gulp.task('build:sources', sequence('clean', 'bower', ['scripts', 'scripts:app', 'styles', 'img'], 'views:sources'));

gulp.task('watch:sources', function () {
    gulp.watch(paths.script, ['scripts']);
    gulp.watch(frontend + 'app/**/*.js', ['scripts:app']);
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.views, ['views:sources']);
    gulp.watch(paths.images, ['img']);
});