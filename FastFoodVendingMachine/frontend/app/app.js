﻿(function () {
    'use strict';
    var serviceBase = "http://localhost:50385/";
    var fastFoodApp = angular.module('fastFoodApp', [
        // Angular modules 
        'ngRoute',
        'ngAnimate',
        'ngResource',
        'ngMessages',
        //// Custom modules 

        //// 3rd Party Modules
        'angular-loading-bar',
        'ngMaterial'
    ]);

    fastFoodApp.constant("config",
        {
            host: serviceBase,
            hostApi: serviceBase + "api/"
        });

    configFunction.$inject = ['$routeProvider', '$httpProvider','$mdDateLocaleProvider', '$mdThemingProvider', '$locationProvider'];

    fastFoodApp.config(configFunction);

    function configFunction($routeProvider, $httpProvider, $mdDateLocaleProvider, $mdThemingProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'homeController',
                templateUrl: '/views/welcome.html'
            })
            .when('/order', {
                controller: 'orderController',
                templateUrl: '/views/order.html'
            })
            .otherwise({
                redirectTo: '/'
            });

        
        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    };
})();