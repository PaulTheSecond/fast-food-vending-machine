(function () {
    'use strict';

    angular
        .module('fastFoodApp')
        .directive('product', productDirective);

    productDirective.$inject = ['$window'];

    function productDirective($window) {
        // Usage:
        //     <product></product>
        // Creates:
        // 
        var directive = {
            restrict: 'E',
            scope: {
                title: "@",
                subtitle: "@",
                data: "=",
                isMeal: "=",
                order: "=",
                isFood: "=",
                total: '=',
                totalChanged: '&onTotalChanged'
            },
            templateUrl: "views/productDirectiveView.html",
            controller: "productDirectiveController"
        };
        return directive;
    }

})();