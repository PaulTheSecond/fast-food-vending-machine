(function () {
    'use strict';

    angular
        .module('fastFoodApp')
        .controller('orderController', orderController);

    orderController.$inject = ['$scope', '$routeParams', '$http', 'config'];

    function orderController($scope, $routeParams, $http, config) {
        $scope.title = 'orderController';


        activate();
        var order;
        var mealItem;
        var cntTosend = 0;
        function activate() {
            $scope.order = {};
            $scope.drinks = undefined;
            $scope.food = undefined;
            $scope.total = 0;
            $scope.total1 = 0;
            $scope.total2 = 0;
            order = {
                number: '123',
                date: new Date(),
                orderItems: []
            };
            mealItem = {};


            $scope.isMeal = ($routeParams.isMeal === '1');

            $scope.onTotalChanged = _onTotalChanged;
            $scope.fillOrder = _fillOrder;

            _initFood();
            _initDrinks();
        }

        function _initFood() {
            $http.get(config.hostApi + 'Food').then(
                function (response) {
                    $scope.food = response.data;
                }, function () {
                });
        }

        function _initDrinks() {
            $http.get(config.hostApi + 'Drink').then(
                function (response) {
                    $scope.drinks = response.data;
                }, function () {
                });
        }

        function _onTotalChanged(total1, total2, isFood) {
            debugger;
            $scope.total1 = !isFood ? total1 || 0 : $scope.total1;
            $scope.total2 = isFood ? total2 || 0 : $scope.total2;
            $scope.total = $scope.total1 + $scope.total2;
        }

        function sendOrder(orderI, isFood, num) {
            if ($scope.isMeal) {
                if (isFood) {
                    mealItem.food = orderI.food;
                    mealItem.foodAddition = orderI.foodAddition;
                    mealItem.foodAdditionQuantity = orderI.foodAdditionQuantity;
                } else {
                    mealItem.drink = orderI.food;
                    mealItem.drinkAddition = orderI.drinkAddition;
                    mealItem.drinkAdditionQuantity = orderI.drinkAdditionQuantity;
                }
            } else {
                orderI.forEach(function(item) {
                    order.orderItems.push(item);
                });
            }

            cntTosend += num;

            if (cntTosend < 2) return;

            $http.post(config.hostApi + 'GetTotals', order).then(function(resp) {
                
            }, function() {});
        }

        _fillOrder()
    }
})();
