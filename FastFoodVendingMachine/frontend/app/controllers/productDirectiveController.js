(function () {
    'use strict';

    angular
        .module('fastFoodApp')
        .controller('productDirectiveController', productDirectiveController);

    productDirectiveController.$inject = ['$scope'];

    function productDirectiveController($scope) {


        activate();

        function activate() {
            $scope.toggle = _toggle;
            $scope.exists = _exists;
            $scope.onProductChange = _onProductChange;
            $scope.onAdditionChanged = _onAdditionChanged;
            $scope.onShugarChamched = _onShugarChamched;
            $scope.send = _send;

            $scope.selectedProduct = undefined;
            $scope.selectedAdditions = undefined;
        }

        function _toggle(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
            }
            calculate();
        };

        function _exists(item, list) {
            return list.indexOf(item) > -1;
        };

        function _onProductChange() {
            $scope.additions = $scope.selectedProduct.additions;
            $scope.selectedAdditions = [];
            calculate();
        }

        function _onAdditionChanged(item) {
            $scope.selectedAdditions = [item];
            calculate();
        }

        function _onShugarChamched() {
            calculate();
        }

        function calculate() {
            debugger;
            var res = 0;
            $scope.selectedAdditions.forEach(function (item) {
                res += item.price * (item.quantity || 1);
            });
            res += $scope.selectedProduct.price;
            $scope.total = res;
            var isF = $scope.isFood;
            var total1 = !isF ? res : null;
            var total2 = isF ? res : null;
            $scope.$parent.$parent.onTotalChanged(total1, total2, isF);
        }

        function _send() {
            var oItems = [];
            $scope.selectedAdditions.forEach(function (item) {
                if ($scope.isFood) {
                    oItems.push({
                        foodAddition: item,
                        foodAdditionQuantity: item.quantity || 1
                    });
                } else {
                    oItems.push({
                        drinkAddition: item,
                        drinkAdditionQuantity: item.quantity || 1
                    });
                }
            });
            if ($scope.isMeal) {
                oItems = oItems[0];
                if ($scope.isFood) {
                    oItems.food = $scope.selectedProduct;
                } else {
                    oItems.drink = $scope.selectedProduct;
                }
            } else {
                if ($scope.isFood) {
                    oItems.push({
                        food: $scope.selectedProduct
                    });
                } else {
                    oItems.push({
                        drink: $scope.selectedProduct
                    });
                }
            }

            $scope.$parent.$parent.fillOrder(oItems, $scope.isFood, 1);
        }
    }
})();
